module MockDiscordGateway where

import qualified Control.Concurrent  as C
import qualified Data.Aeson          as Aeson
import qualified Network.WebSockets  as WS

import           Web.Discord.Logging
import           Web.Discord.Types

runMockDiscordGatewayServer = do
  logDebug "Mock Discord Gateway Server => Listening on port 2020."
  WS.runServer "0.0.0.0" 2020 mockApp

mockApp :: WS.ServerApp
mockApp pendingConn = do
  conn <- WS.acceptRequest pendingConn
  logDebug "New connection accepted"
  -- on connection, immediately send back 'Hello'
  logDebug "sending Hello"
  sendPayload conn $ Hello 12300
  logDebug "blocking for 5 secs.."
  C.threadDelay (5 * 10 ^ 6)
  logDebug "sending Reconnect"
  sendPayload conn Reconnect
  logDebug "blocking for 10 secs.."
  C.threadDelay (10 * 10 ^ 6)

-- | Send a 'GatewaySendable' payload over the websocket connection
sendPayload :: WS.Connection -> GatewayReceivable -> IO ()
sendPayload conn payload = do
  WS.sendTextData conn (Aeson.encode payload)
