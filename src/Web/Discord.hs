{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeApplications    #-}

module Web.Discord where

import           Control.Monad.IO.Class
import           Control.Monad.Reader
import           Data.Text              (Text)
import           Network.HTTP.Req       ((/:))

import qualified Data.Aeson             as A
import qualified Data.Aeson.Casing      as A
import qualified Data.Aeson.TH          as A
import qualified Data.ByteString        as B
import qualified Data.Text              as T
import qualified Data.Text.Encoding     as T
import qualified Network.HTTP.Req       as Req

import           Web.Discord.Gateway
import           Web.Discord.Logging
import           Web.Discord.Types

data CreateMessage
  = CreateMessage
  { cmContent :: !Text
  , cmEmbed   :: !(Maybe Embed)
  } deriving (Show, Eq)

$(A.deriveJSON (A.aesonDrop 2 A.snakeCase) {A.omitNothingFields = True}
  ''CreateMessage)

bsToTxt :: B.ByteString -> Text
bsToTxt = T.decodeUtf8

txtToBs :: Text -> B.ByteString
txtToBs = T.encodeUtf8

tshow :: Show a => a -> Text
tshow = T.pack . show

baseURL :: Req.Url 'Req.Https
baseURL = Req.https "discordapp.com" /: "api"

gatewayURL :: Req.Url 'Req.Https
gatewayURL = baseURL /: "gateway" /: "bot"

channelsURL :: Text -> Req.Url 'Req.Https
channelsURL channelId = baseURL /: "channels" /: channelId

messagesURL :: Text -> Req.Url 'Req.Https
messagesURL channelId = baseURL /: "channels" /: channelId /: "messages"

reactionURL :: Text -> Text -> Text -> Req.Url 'Req.Https
reactionURL channelId msgId emoji =
  messagesURL channelId /: msgId /: "reactions" /: emoji /: "@me"

typingURL :: Text -> Req.Url 'Req.Https
typingURL channelId = channelsURL channelId /: "typing"

getGateway :: (MonadReader DiscordContext m, MonadIO m) => m Text
getGateway = do
  resp <- mkGetRequest gatewayURL mempty
  return $ T.drop 6 $ guUrl resp

mkHeaders :: AuthToken -> Req.Option scheme
mkHeaders token =
  foldr (\(k, v) opts -> opts <> Req.header k v) mempty headers
  where
    headers =
      [ ("Authorization", txtToBs $ mkAuthToken token)
      , ("User-Agent", "pipit-bot (io.bot.pipit, v0.1)")
      , ("Content-Type", "application/json")
      , ("Accept-Type", "application/json")
      ]

snowflakeToTxt :: Snowflake -> Text
snowflakeToTxt = T.pack . snowflakeToString

addReaction
  :: (MonadReader DiscordContext m, MonadIO m)
  => ChannelId -> MessageId -> Text -> m ()
addReaction (ChannelId channelId) (MessageId msgId) emoji = do
  token <- asks discordContextAuthToken
  let headers = mkHeaders token
      url = reactionURL (snowflakeToTxt channelId) (snowflakeToTxt msgId) emoji
  Req.runReq Req.defaultHttpConfig $
    void $ Req.req Req.PUT url Req.NoReqBody Req.ignoreResponse headers

makeMessage :: Text -> Maybe Embed -> [UserId] -> CreateMessage
makeMessage text embed users =
  let mentions = T.intercalate ", " $
                 map (\(UserId u) -> "<@" <> snowflakeToTxt u <> ">") users
  in CreateMessage (mentions <> " " <> text) embed

makeEmbed :: Text -> Text -> [EmbedField] -> Integer -> Embed
makeEmbed title url fields color =
  Embed
  { embedAuthor      = Nothing
  , embedTitle       = Just title
  -- Title of the embed
  , embedUrl         = Just url
  -- URL of embed
  , embedThumbnail   = Nothing
  -- Thumbnail in top-right
  , embedDescription = Nothing
  -- Description of embed
  , embedFields      = fields
  -- Fields of the embed
  , embedImage       = Nothing
  , embedFooter      = Nothing
  , embedColor       = Just color
  -- The embed color
  , embedTimestamp   = Nothing
  -- The time of the embed content
  , embedType        = Just "rich"
  -- Type of embed (Always "rich" for users)
  , embedVideo       = Nothing
  -- Only present for "video" types
  , embedProvider    = Nothing
  -- Only present for "video" types
  }

sendMessage_
  :: (MonadReader DiscordContext m, MonadIO m)
  =>  ChannelId -> Text -> [UserId] -> Maybe Embed -> m ()
sendMessage_ cid t m e = void $ sendMessage cid t m e

-- TODO: handle discord exceptions/errors
-- 1. content - must be 2000 characters or less
sendMessage
  :: (MonadReader DiscordContext m, MonadIO m)
  => ChannelId -> Text -> [UserId] -> Maybe Embed -> m Message
sendMessage (ChannelId channelId) text mentions embed = do
  when (T.length text > 2000) $ do
    logError "sendMessage content greater than 2000. Aborting.."
    return ()
  let payload = makeMessage text embed mentions
  mkPostRequest (messagesURL $ snowflakeToTxt channelId) mempty payload

data MessageCursorDirection
  = MCDirectionBefore
  | MCDirectionAfter
  | MCDirectionAround
  deriving (Show)

directionToTxt :: MessageCursorDirection -> Text
directionToTxt = \case
  MCDirectionBefore -> "before"
  MCDirectionAfter  -> "after"
  MCDirectionAround -> "around"

getMessages
  :: (MonadReader DiscordContext m, MonadIO m)
  => ChannelId -> MessageId -> MessageCursorDirection -> Int -> m [Message]
getMessages (ChannelId channelId) (MessageId msgId) direction limit = do
  let options = directionToTxt direction Req.=: snowflakeToTxt msgId
             <> ("limit" :: Text) Req.=: limit
  mkGetRequest (messagesURL $ snowflakeToTxt channelId) options


triggerTypingIndicator
  :: (MonadReader DiscordContext m, MonadIO m)
  => ChannelId -> m ()
triggerTypingIndicator (ChannelId channelId) = do
  token <- asks discordContextAuthToken
  let url = (typingURL $ snowflakeToTxt channelId)
      headers = mkHeaders token
  Req.runReq Req.defaultHttpConfig $
    void $ Req.req Req.POST url Req.NoReqBody Req.ignoreResponse headers


---------------------------------------------------------

-- * Helper functions for HTTP requests to the discord API

mkGetRequest
  :: (MonadReader DiscordContext m, MonadIO m, A.FromJSON a)
  => Req.Url scheme -> Req.Option scheme -> m a
mkGetRequest url userOptions = do
  token <- asks discordContextAuthToken
  let headers = mkHeaders token
      options = userOptions <> headers
  Req.runReq Req.defaultHttpConfig $ do
    resp <- Req.req Req.GET url Req.NoReqBody Req.jsonResponse options
    return $ Req.responseBody resp

mkPostRequest
  :: (MonadReader DiscordContext m, MonadIO m, A.ToJSON a, A.FromJSON b)
  => Req.Url scheme -> Req.Option scheme -> a -> m b
mkPostRequest url userOptions payload = do
  token <- asks discordContextAuthToken
  let headers = mkHeaders token
      options = userOptions <> headers
      body = Req.ReqBodyJson payload
  Req.runReq Req.defaultHttpConfig $ do
    -- logDebug $ "=======> Request Object: "
    -- liftIO $ BL.putStrLn $ A.encode payload
    resp <- Req.req Req.POST url body Req.jsonResponse options
    -- logDebug $ "=======> Response Object: "
    -- liftIO $ print resp
    return $ Req.responseBody resp
