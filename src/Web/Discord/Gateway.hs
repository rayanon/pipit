module Web.Discord.Gateway where

import           Control.Concurrent      (forkIO, killThread, threadDelay)
import           Control.Concurrent.Chan (Chan)
import           Control.Monad.IO.Class
import           Control.Monad.Reader

import qualified Control.Concurrent.Chan as Chan
import qualified Data.Aeson              as Aeson
import qualified Data.ByteString         as B
import qualified Data.ByteString.Char8   as BC
import qualified Data.IORef              as IORef
import qualified Data.Text               as T
import qualified Network.WebSockets      as WS
import qualified Wuss                    as WS

import           Web.Discord.Logging
import           Web.Discord.Types

-- | Concurrent channel, on which the gateway events are received
type EventChannel = Chan B.ByteString

-- | Custom monad for the client/API
type DiscordM = ReaderT DiscordContext IO

-- | A collection of global environment that is required for operating in 'DiscordM'
data DiscordContext
  = DiscordContext
  { discordContextAuthToken         :: !AuthToken
  , discordContextGatewayConnection :: !WS.Connection
  }

-- | User provided handler or callback functions for various events
data DiscordHandlers
  = DiscordHandlers
  { discordOnMessage  :: forall m. (MonadReader DiscordContext m, MonadIO m) => Message -> m ()
  , discordOnReaction :: forall m. (MonadReader DiscordContext m, MonadIO m) => Message -> m ()
  }

-- | Send a 'GatewaySendable' payload over the websocket connection
sendPayload :: (MonadReader DiscordContext m, MonadIO m) => GatewaySendable -> m ()
sendPayload payload = do
  conn <- asks discordContextGatewayConnection
  liftIO $ WS.sendTextData conn (Aeson.encode payload)

-- | Entry point for the client-facing API. First argument is 'String' for
-- convenience to the user
runDiscordWSClient :: String -> DiscordHandlers -> IO ()
runDiscordWSClient authTokenStr handlers = do
  let authToken = AuthToken $ T.pack authTokenStr
  eventStream <- Chan.newChan
  signalRef <- IORef.newIORef (Signal False)

  forever $ do
    logDebug "Running our websocket client"
    -- WS.runClient "localhost" 2020 "/?v=6&encoding=json" $ \conn -> do
    WS.runSecureClient "gateway.discord.gg" 443 "/?v=6&encoding=json" $ \conn -> do
      websocketClient eventStream signalRef authToken handlers conn
    logDebug "Websocket client closed (probably due to Reconnect). Resetting signal and reconnecting.."
    liftIO $ threadDelay (10 ^ 6 * 3)
    IORef.writeIORef signalRef (Signal False)

newtype Signal = Signal { connectionDisconnect :: Bool }

websocketClient :: EventChannel -> IORef.IORef Signal -> AuthToken -> DiscordHandlers -> WS.Connection -> IO ()
websocketClient eventStream signalRef authToken handlers conn = do
  logDebug "Connected!"

  -- Fork a thread that writes incoming data from the connection to a channel
  recvrThreadId <- forkIO $ recieveMsg

  -- in this thread, keep reading from the event stream
  runReaderT readGatewayMsg (DiscordContext authToken conn)
  liftIO $ WS.sendClose conn ("bye!" :: T.Text)
  killThread recvrThreadId
  where
    recieveMsg = do
      signal <- liftIO $ IORef.readIORef signalRef
      case connectionDisconnect signal of
        True -> return ()
        False -> do
          msg <- liftIO $ WS.receiveData conn
          liftIO $ Chan.writeChan eventStream msg
          recieveMsg

    readGatewayMsg :: (MonadReader DiscordContext m, MonadIO m) => m ()
    readGatewayMsg = do
      signal <- liftIO $ IORef.readIORef signalRef
      case connectionDisconnect signal of
        True -> return ()
        False -> do
          msg <- liftIO $ Chan.readChan eventStream
          case Aeson.eitherDecodeStrict msg of
            Left e  -> logError $ "Error parsing gateway response: " <> BC.pack (show e)
            Right v -> do
              logDebug $ "Gateway Event: " <> BC.pack (gatewayReceivableTypeString v)
              processGatewayEvent signalRef handlers v
          readGatewayMsg

processGatewayEvent
  :: (MonadReader DiscordContext m, MonadIO m)
  => IORef.IORef Signal -> DiscordHandlers -> GatewayReceivable -> m ()
processGatewayEvent signalRef handlers = \case
  Dispatch event _int   -> processDispatchEvent handlers event
  HeartbeatRequest _int -> return ()
  Reconnect             -> do
    logDebug "!!!!!!!!!! Received Reconnect from Discord !!!!!!!!!!!!!"
    liftIO $ IORef.writeIORef signalRef (Signal True)
  InvalidSession _bool  -> return ()
  Hello interval        -> sendIdentify >> startHearbeatThread interval
  HeartbeatAck          -> return ()
  ParseError _err       -> return ()

sendIdentify :: (MonadReader DiscordContext m, MonadIO m) => m ()
sendIdentify = do
  authToken <- asks discordContextAuthToken
  sendPayload $ Identify authToken False 50 (0,1)

startHearbeatThread :: (MonadReader DiscordContext m, MonadIO m) => Int -> m ()
startHearbeatThread interval = do
  ctx <- ask
  _threadId <- liftIO $ forkIO $ forever $ do
    logDebug "Sending heartbeat now.."
    flip runReaderT ctx $ sendPayload $ Heartbeat 0
    liftIO $ threadDelay (1000 * interval)
  return ()

processDispatchEvent
  :: (MonadReader DiscordContext m, MonadIO m)
  => DiscordHandlers -> Event -> m ()
processDispatchEvent DiscordHandlers{..} ev = do
  ctx <- ask
  case ev of
    EventReady _gwVersion meUser _channels _guildsUnavailable sessionId -> return ()
    EventResumed _ -> return ()
    EventMessageCreate message -> liftIO $ flip runReaderT ctx $ discordOnMessage message
    _ -> logDebug "Unsupported event received. Skipping."
  -- EventChannelCreate              !Channel
  -- EventChannelUpdate              !Channel
  -- EventChannelDelete              !Channel
  -- EventChannelPinsUpdate          !ChannelId !(Maybe UTCTime)
  -- EventGuildCreate                !Guild !GuildInfo
  -- EventGuildUpdate                !Guild
  -- EventGuildDelete                !GuildUnavailable
  -- EventGuildBanAdd                !GuildId !User
  -- EventGuildBanRemove             !GuildId !User
  -- EventGuildEmojiUpdate           !GuildId ![Emoji]
  -- EventGuildIntegrationsUpdate    !GuildId
  -- EventGuildMemberAdd             !GuildId !GuildMember
  -- EventGuildMemberRemove          !GuildId !User
  -- EventGuildMemberUpdate          !GuildId ![RoleId] User (Maybe T.Text)
  -- EventGuildMemberChunk           !GuildId ![GuildMember]
  -- EventGuildRoleCreate            !GuildId !Role
  -- EventGuildRoleUpdate            !GuildId !Role
  -- EventGuildRoleDelete            !GuildId !RoleId
  -- EventMessageUpdate              !ChannelId !MessageId
  -- EventMessageDelete              !ChannelId !MessageId
  -- EventMessageDeleteBulk          !ChannelId ![MessageId]
  -- EventMessageReactionAdd         !ReactionInfo
  -- EventMessageReactionRemove      !ReactionInfo
  -- EventMessageReactionRemoveAll   !ChannelId !MessageId
  -- EventMessageReactionRemoveEmoji !ReactionRemoveInfo
  -- EventPresenceUpdate             !PresenceInfo
  -- EventTypingStart                !TypingInfo
  -- EventUserUpdate                 !User
