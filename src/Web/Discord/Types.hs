-- | Provides types and encoding/decoding code. Types should be identical to those provided
--   in the Discord API documentation.odule Discord.Internal.Types
module Web.Discord.Types
  ( module Web.Discord.Types.Common
  , module Web.Discord.Types.Channel
  , module Web.Discord.Types.Embed
  , module Web.Discord.Types.Events
  , module Web.Discord.Types.Gateway
  , module Web.Discord.Types.Guild
  , module Web.Discord.Types.User
  ) where

import           Web.Discord.Types.Channel
import           Web.Discord.Types.Common
import           Web.Discord.Types.Embed
import           Web.Discord.Types.Events
import           Web.Discord.Types.Gateway
import           Web.Discord.Types.Guild
import           Web.Discord.Types.User
