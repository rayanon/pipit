{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}

-- | Data structures pertaining to Discord Channels
module Web.Discord.Types.Channel where

import           Control.Applicative      (empty)
import           Data.Aeson
import           Data.Aeson.Casing
import           Data.Aeson.TH
import           Data.Monoid              ((<>))
import           Data.Text                (Text)
import           Data.Time.Clock

import qualified Data.Text                as T

import           Web.Discord.Types.Common
import           Web.Discord.Types.Embed
import           Web.Discord.Types.User   (User (..))


-- | Permission overwrites for a channel.
data Overwrite
  = Overwrite
  { overwriteId    :: !OverwriteId
  -- ^ 'Role' or 'User' id
  , overwriteType  :: !T.Text
  -- ^ Either "role" or "member
  , overwriteAllow :: !Integer
  -- ^ Allowed permission bit set
  , overwriteDeny  :: !Integer
  -- ^ Denied permission bit set
  } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("overwrite" :: String)) snakeCase) ''Overwrite)

data TextChannel
  = TextChannel
  { textChannelId                   :: !ChannelId
  -- ^ The id of the channel (will be equal to the guild if it's the "general" channel).
  , textChannelGuildId              :: !(Maybe GuildId)
  -- ^ The id of the guild.
  , textChannelName                 :: !T.Text
  -- ^ The name of the guild (2 - 1000 characters).
  , textChannelPosition             :: !Integer
  -- ^ The storing position of the channel.
  , textChannelPermissionOverwrites :: ![Overwrite]
  -- ^ An array of permission 'Overwrite's
  , textChannelTopic                :: !(Maybe T.Text)
  -- ^ The topic of the channel. (0 - 1024 chars).
  , textChannelLastMessageId        :: !(Maybe MessageId)
  -- ^ The id of the last message sent in the channel
  } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("textChannel" :: String)) snakeCase) ''TextChannel)

data VoiceChannel
  = VoiceChannel
  { voiceChannelId                   :: !ChannelId
  , voiceChannelGuildId              :: !(Maybe GuildId)
  , voiceChannelName                 :: !T.Text
  , voiceChannelPosition             :: !Integer
  , voiceChannelPermissionOverwrites :: ![Overwrite]
  , voiceChannelBitrate              :: !Integer
  -- ^ The bitrate (in bits) of the channel.
  , voiceChannelUserLimit            :: !Integer
  -- ^ The user limit of the voice channel.
  } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("voiceChannel" :: String)) snakeCase) ''VoiceChannel)

data DirectMessageChannel
  = DirectMessageChannel
  { dmChannelId            :: !ChannelId
  , dmChannelRecipients    :: ![User]
  -- ^ The 'User' object(s) of the DM recipient(s).
  , dmChannelLastMessageId :: !(Maybe MessageId)
  } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("dmChannel" :: String)) snakeCase) ''DirectMessageChannel)

data GroupDMChannel
  = GroupDMChannel
  { groupDMchannelId            :: !ChannelId
  , groupDMchannelRecipients    :: ![User]
  , groupDMchannelLastMessageId :: !(Maybe MessageId)
  } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("groupDMChannel" :: String)) snakeCase) ''GroupDMChannel)

data GuildCategoryChannel
 = GuildCategoryChannel
 { categoryChannelId      :: !ChannelId
 , categoryChannelGuildId :: !(Maybe GuildId)
 } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("categoryChannel" :: String)) snakeCase) ''GuildCategoryChannel)

-- | Guild channels represent an isolated set of users and messages in a Guild (Server)
data Channel
  = ChannelText !TextChannel
  -- ^ A text channel in a guild.
  | ChannelVoice !VoiceChannel
  -- ^ A voice channel in a guild.
  | ChannelDirectMessage !DirectMessageChannel
  -- ^ DM Channels represent a one-to-one conversation between two users, outside the scope
  --   of guilds
  | ChannelGroupDM !GroupDMChannel
  | ChannelGuildCategory !GuildCategoryChannel
  deriving (Show, Eq)

instance FromJSON Channel where
  parseJSON v@(Object o) = do
    type' :: Int <- (o .: "type")
    case type' of
      0 -> ChannelText <$> parseJSON v
      1 -> ChannelDirectMessage <$> parseJSON v
      2 -> ChannelVoice <$> parseJSON v
      3 -> ChannelGroupDM <$> parseJSON v
      4 -> ChannelGuildCategory <$> parseJSON v
      _ -> fail ("Channel::parseJSON: unknown channel type:" <> show type')

  parseJSON _ = fail "Channel::parseJSON: expecting object for Channel"

instance ToJSON Channel where
  toJSON = \case
    ChannelText v          -> toJSON v
    ChannelDirectMessage v -> toJSON v
    ChannelVoice v         -> toJSON v
    ChannelGroupDM v       -> toJSON v
    ChannelGuildCategory v -> toJSON v



-- | If the channel is part of a guild (has a guild id field)
channelIsInGuild :: Channel -> Bool
channelIsInGuild c = case c of
        ChannelGuildCategory _ -> True
        ChannelText _          -> True
        ChannelVoice _         -> True
        _                      -> False


-- | A nonce type could be a string or integer
newtype Nonce = Nonce {unNonce :: Text }
  deriving (Show, Eq)

instance FromJSON Nonce where
  parseJSON (String nonce) = pure $ Nonce nonce
  parseJSON (Number nonce) = pure . Nonce . T.pack . show $ nonce
  parseJSON _              = fail "could not parse Nonce"


-- | Represents information about a message in a Discord channel.
data Message
  = Message
  { messageId           :: !MessageId
  -- ^ The id of the message
  , messageChannel      :: !ChannelId
  -- ^ Id of the channel the message was sent in
  , messageAuthor       :: !User
  -- ^ The 'User' the message was sent by
  , messageText         :: !Text
  -- ^ Contents of the message
  , messageTimestamp    :: !UTCTime
  -- ^ When the message was sent
  , messageEdited       :: !(Maybe UTCTime)
  -- ^ When/if the message was edited
  , messageTts          :: !Bool
  -- ^ Whether this message was a TTS message
  , messageEveryone     :: !Bool
  -- ^ Whether this message mentions everyone
  , messageMentions     :: ![User]
  -- ^ 'User's specifically mentioned in the message
  , messageMentionRoles :: ![RoleId]
  -- ^ 'Role's specifically mentioned in the message
  , messageAttachments  :: ![Attachment]
  -- ^ Any attached files
  , messageEmbeds       :: ![Embed]
  -- ^ Any embedded content
  , messageNonce        :: !(Maybe Nonce)
  -- ^ Used for validating if a message was sent
  , messagePinned       :: !Bool
  -- ^ Whether this message is pinned
  , messageGuild        :: !(Maybe GuildId)
  -- ^ The guild the message went to
  } deriving (Show, Eq)

instance FromJSON Message where
  parseJSON = withObject "Message" $ \o ->
    Message <$> o .:  "id"
            <*> o .:  "channel_id"
            <*> (do isW <- o .:? "webhook_id"
                    a <- o .: "author"
                    case isW :: Maybe WebhookId of
                      Nothing -> pure a
                      Just _  -> pure $ a { userIsWebhook = True })
            <*> o .:? "content" .!= ""
            <*> o .:? "timestamp" .!= epochTime
            <*> o .:? "edited_timestamp"
            <*> o .:? "tts" .!= False
            <*> o .:? "mention_everyone" .!= False
            <*> o .:? "mentions" .!= []
            <*> o .:? "mention_roles" .!= []
            <*> o .:? "attachments" .!= []
            <*> o .:  "embeds"
            <*> o .:? "nonce"
            <*> o .:? "pinned" .!= False
            <*> o .:? "guild_id" .!= Nothing

-- | Represents an attached to a message file.
data Attachment = Attachment
  { attachmentId       :: !Snowflake
  -- ^ Attachment id
  , attachmentFilename :: !Text
  -- ^ Name of attached file
  , attachmentSize     :: !Integer
  -- ^ Size of file (in bytes)
  , attachmentUrl      :: !Text
  -- ^ Source of file
  , attachmentProxyUrl :: !Text
  -- ^ Proxied url of file
  , attachmentHeight   :: !(Maybe Integer)
  -- ^ Height of file (if image)
  , attachmentWidth    :: !(Maybe Integer)
  -- ^ Width of file (if image)
  } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("attachment" :: String)) snakeCase) ''Attachment)
