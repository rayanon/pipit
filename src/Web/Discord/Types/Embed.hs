{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}

-- | Data structures pertaining to Discord Embed
module Web.Discord.Types.Embed where

import           Data.Aeson
import           Data.Aeson.Casing
import           Data.Aeson.TH
import           Data.Text         (Text)
import           Data.Time.Clock

import qualified Data.ByteString   as B
import qualified Data.Text         as T


-- | An embed attached to a message.
data Embed
  = Embed
  { embedAuthor      :: Maybe EmbedAuthor
  , embedTitle       :: Maybe Text
  -- ^ Title of the embed
  , embedUrl         :: Maybe Text
  -- ^ URL of embed
  , embedThumbnail   :: Maybe EmbedThumbnail
  -- ^ Thumbnail in top-right
  , embedDescription :: Maybe Text
  -- ^ Description of embed
  , embedFields      :: [EmbedField]
  -- ^ Fields of the embed
  , embedImage       :: Maybe EmbedImage
  , embedFooter      :: Maybe EmbedFooter
  , embedColor       :: Maybe Integer
  -- ^ The embed color
  , embedTimestamp   :: Maybe UTCTime
  -- ^ The time of the embed content
  , embedType        :: Maybe Text
  -- ^ Type of embed (Always "rich" for users)
  , embedVideo       :: Maybe EmbedVideo
  -- ^ Only present for "video" types
  , embedProvider    :: Maybe EmbedProvider
  -- ^ Only present for "video" types
  } deriving (Show, Eq)


data EmbedThumbnail
  = EmbedThumbnail
  { embedThumbnailUrl      :: !(Maybe Text)
  , embedThumbnailProxyUrl :: !(Maybe Text)
  , embedThumbnailHeight   :: !(Maybe Integer)
  , embedThumbnailWidth    :: !(Maybe Integer)
  } deriving (Show, Eq)

data EmbedVideo
  = EmbedVideo
  { embedVideoUrl    :: !(Maybe Text)
  , embedVideoHeight :: !(Maybe Integer)
  , embedVideoWidth  :: !(Maybe Integer)
  } deriving (Show, Eq)

data EmbedImage
  = EmbedImage
  { embedImageUrl      :: !(Maybe Text)
  , embedImageProxyUrl :: !(Maybe Text)
  , embedImageHeight   :: !(Maybe Integer)
  , embedImageWidth    :: !(Maybe Integer)
  } deriving (Show, Eq)

data EmbedProvider
  = EmbedProvider
  { embedProviderName :: !(Maybe Text)
  , embedProviderUrl  :: !(Maybe Text)
  } deriving (Show, Eq)

data EmbedAuthor
  = EmbedAuthor
  { embedAuthorName         :: !(Maybe Text)
  , embedAuthorUrl          :: !(Maybe Text)
  , embedAuthorIconUrl      :: !(Maybe Text)
  , embedAuthorProxyIconUrl :: !(Maybe Text)
  } deriving (Show, Eq)

data EmbedFooter
  = EmbedFooter
  { embedFooterText         :: !(Text)
  , embedFooterIconUrl      :: !(Maybe Text)
  , embedFooterProxyIconUrl :: !(Maybe Text)
  } deriving (Show, Eq)

data EmbedField
  = EmbedField
  { embedFieldName   :: !(Text)
  , embedFieldValue  :: !(Text)
  , embedFieldInline :: !(Maybe Bool)
  } deriving (Show, Eq)


$(deriveJSON (aesonDrop (length ("embedThumbnail" :: String)) snakeCase) ''EmbedThumbnail)
$(deriveJSON (aesonDrop (length ("embedImage" :: String)) snakeCase) ''EmbedImage)
$(deriveJSON (aesonDrop (length ("embedVideo" :: String)) snakeCase) ''EmbedVideo)
$(deriveJSON (aesonDrop (length ("embedProvider" :: String)) snakeCase) ''EmbedProvider)
$(deriveJSON (aesonDrop (length ("embedAuthor" :: String)) snakeCase) ''EmbedAuthor)
$(deriveJSON (aesonDrop (length ("embedFooter" :: String)) snakeCase) ''EmbedFooter)
$(deriveJSON (aesonDrop (length ("embedField" :: String)) snakeCase) ''EmbedField)

$(deriveToJSON (aesonDrop (length ("embed" :: String)) snakeCase) ''Embed)

instance FromJSON Embed where
  parseJSON = withObject "Embed" $ \o -> do
    embedAuthor      <- o .:? "author"
    embedTitle       <- o .:? "title"
    embedUrl         <- o .:? "url"
    embedThumbnail   <- o .:? "thumbnail"
    embedDescription <- o .:? "description"
    embedFields      <- o .:? "fields" .!= []
    embedImage       <- o .:? "image"
    embedFooter      <- o .:? "footer"
    embedColor       <- o .:? "color"
    embedTimestamp   <- o .:? "timestamp"
    embedType        <- o .:? "type"
    embedVideo       <- o .:? "video"
    embedProvider    <- o .:? "provider"
    return $ Embed{..}

createEmbed :: CreateEmbed -> Embed
createEmbed CreateEmbed{..} =
  Embed { embedAuthor      = Just embedAuthor
        , embedTitle       = emptyMaybe createEmbedTitle
        , embedUrl         = emptyMaybe createEmbedUrl
        , embedThumbnail   = Just embedThumbnail
        , embedDescription = emptyMaybe createEmbedDescription
        , embedFields      = createEmbedFields
        , embedImage       = Just embedImage
        , embedFooter      = Just embedFooter
        , embedColor       = Nothing
        , embedTimestamp   = Nothing

        -- can't set these
        , embedType        = Nothing
        , embedVideo       = Nothing
        , embedProvider    = Nothing
        }

  where
    -- emptyMaybe :: Text -> Maybe Text
    emptyMaybe t = if T.null t then Nothing else Just t

    -- embedImageToUrl :: Text -> CreateEmbedImage -> Text
    embedImageToUrl place cei =
      case cei of
        CreateEmbedImageUrl t    -> t
        CreateEmbedImageUpload _ -> "attachment://" <> place <> ".png"

    embedAuthor = EmbedAuthor (emptyMaybe createEmbedAuthorName)
                              (emptyMaybe createEmbedAuthorUrl)
                              (embedImageToUrl "author" <$> createEmbedAuthorIcon)
                              Nothing
    embedImage = EmbedImage (embedImageToUrl "image" <$> createEmbedImage)
                 Nothing Nothing Nothing
    embedThumbnail = EmbedThumbnail (embedImageToUrl "thumbnail" <$> createEmbedThumbnail) Nothing Nothing Nothing
    embedFooter = EmbedFooter createEmbedFooterText
                  (embedImageToUrl "footer" <$> createEmbedFooterIcon)
                  Nothing


data CreateEmbed
  = CreateEmbed
  { createEmbedAuthorName  :: !Text
  , createEmbedAuthorUrl   :: !Text
  , createEmbedAuthorIcon  :: !(Maybe CreateEmbedImage)
  , createEmbedTitle       :: !Text
  , createEmbedUrl         :: !Text
  , createEmbedThumbnail   :: !(Maybe CreateEmbedImage)
  , createEmbedDescription :: !Text
  , createEmbedFields      :: ![EmbedField]
  , createEmbedImage       :: !(Maybe CreateEmbedImage)
  , createEmbedFooterText  :: !Text
  , createEmbedFooterIcon  :: !(Maybe CreateEmbedImage)
  --, createEmbedColor       :: Maybe Text
  --, createEmbedTimestamp   :: Maybe UTCTime
  } deriving (Show, Eq)

data CreateEmbedImage
  = CreateEmbedImageUrl !Text
  | CreateEmbedImageUpload !B.ByteString
  deriving (Show, Eq)

-- instance Default CreateEmbed where
--  def = CreateEmbed "" "" Nothing "" "" Nothing "" [] Nothing "" Nothing -- Nothing Nothing
