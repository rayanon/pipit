{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeApplications    #-}

-- | Data structures pertaining to Discord User
module Web.Discord.Types.User where

import           Data.Aeson
import           Data.Aeson.Casing
import           Data.Aeson.TH
import           Data.Text                (Text)

import           Web.Discord.Types.Common

import qualified Data.Text                as T

-- | Represents information about a user.
data User
  = User
  { userId            :: !UserId
  -- ^ The user's id.
  , userUsername      :: !Text
  -- ^ The user's username (not unique)
  , userDiscriminator :: !Text
  -- ^ The user's 4-digit discord-tag.
  , userAvatar        :: !(Maybe Text)
  -- ^ The user's avatar hash.
  , userIsBot         :: !Bool
  -- ^ User is an OAuth2 application.
  , userIsWebhook     :: !Bool
  -- ^ User is a webhook
  , userMfaEnabled    :: !(Maybe Bool)
  -- ^ User has two factor authentication enabled on the account.
  , userVerified      :: !(Maybe Bool)
  -- ^ Whether the email has been verified.
  , userEmail         :: !(Maybe Text)
  -- ^ The user's email.
  } deriving (Show, Eq)

userUid :: User -> Text
userUid u = userUsername u <> "#" <> userDiscriminator u

instance FromJSON User where
  parseJSON = withObject "User" $ \o ->
    User <$> o .:  "id"
         <*> o .:  "username"
         <*> o .:  "discriminator"
         <*> o .:? "avatar"
         <*> o .:? "bot" .!= False
         <*> pure False -- webhook
         <*> o .:? "mfa_enabled"
         <*> o .:? "verified"
         <*> o .:? "email"

-- TODO: why does type applications not work?
-- >>> (length "user" @String)
$(deriveToJSON (aesonDrop (length ("user" :: String)) snakeCase) ''User)

data Webhook
  = Webhook
  { webhookId        :: !WebhookId
  , webhookToken     :: !Text
  , webhookChannelId :: !ChannelId
  } deriving (Show, Eq)

$(deriveJSON (aesonDrop (length ("webhook" :: String)) snakeCase) ''Webhook)

data ConnectionObject
  = ConnectionObject
  { connectionObjectId                     :: !Text
  , connectionObjectName                   :: !Text
  , connectionObjectType                   :: !Text
  , connectionObjectRevoked                :: !Bool
  , connectionObjectIntegrations           :: ![IntegrationId]
  , connectionObjectVerified               :: !Bool
  , connectionObjectFriendSyncOn           :: !Bool
  , connectionObjectShownInPresenceUpdates :: !Bool
  , connectionObjectVisibleToOthers        :: !Bool
  } deriving (Show, Eq)

instance FromJSON ConnectionObject where
  parseJSON = withObject "ConnectionObject" $ \o -> do
    integrations <- o .: "integrations"
    ConnectionObject <$> o .: "id"
               <*> o .: "name"
               <*> o .: "type"
               <*> o .: "revoked"
               <*> sequence (map (.: "id") integrations)
               <*> o .: "verified"
               <*> o .: "friend_sync"
               <*> o .: "show_activity"
               <*> ( (==) (1 :: Int) <$> o .: "visibility")
