{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData        #-}
{-# LANGUAGE TemplateHaskell   #-}
{-# LANGUAGE TypeApplications  #-}

-- | Data structures needed for interfacing with the Websocket
--   Gateway
module Web.Discord.Types.Gateway where

import           Data.Aeson
import           Data.Maybe               (fromMaybe)
import           Data.Time                (UTCTime)
import           Data.Time.Clock.POSIX    (utcTimeToPOSIXSeconds)
import           System.Info
import           Text.Read                (readMaybe)

import qualified Data.Aeson.Casing        as J
import qualified Data.Aeson.TH            as J
import qualified Data.Text                as T

import           Web.Discord.Types.Common
import           Web.Discord.Types.Events

data GatewayUrl
  = GatewayUrl
  { guUrl    :: !T.Text
  , guShards :: !Int
  } deriving (Show)

$(J.deriveFromJSON (J.aesonDrop (length ("gu" :: String)) J.snakeCase) ''GatewayUrl)

-- | Represents data sent and received with Discord servers
data GatewayReceivable
  = Dispatch !Event !Integer
  | HeartbeatRequest !Integer
  | Reconnect
  | InvalidSession !Bool
  | Hello !Int
  | HeartbeatAck
  | ParseError !T.Text
  deriving (Show, Eq)

gatewayReceivableTypeString :: GatewayReceivable -> String
gatewayReceivableTypeString = \case
  Dispatch ev _       -> "Dispatch " <> eventString ev
  HeartbeatRequest _  -> "HeartbeatRequest"
  Reconnect           -> "Reconnect"
  InvalidSession _    -> "InvalidSession"
  Hello _             -> "Hello"
  HeartbeatAck        -> "HeartbeatAck"
  ParseError _        -> "ParseError"

data GatewaySendable
  = Heartbeat !Integer
  | Identify !AuthToken !Bool !Integer !(Int, Int)
  | Resume !T.Text !T.Text !Integer
  | RequestGuildMembers !RequestGuildMembersOpts
  | UpdateStatus !UpdateStatusOpts
  | UpdateStatusVoice !UpdateStatusVoiceOpts
  deriving (Show, Eq)

data RequestGuildMembersOpts
  = RequestGuildMembersOpts
  { requestGuildMembersOptsGuildId           :: !GuildId
  , requestGuildMembersOptsNamesStartingWith :: !T.Text
  , requestGuildMembersOptsLimit             :: !Integer
  } deriving (Show, Eq)

data UpdateStatusVoiceOpts
  = UpdateStatusVoiceOpts
  { updateStatusVoiceOptsGuildId   :: !GuildId
  , updateStatusVoiceOptsChannelId :: !(Maybe ChannelId)
  , updateStatusVoiceOptsIsMuted   :: !Bool
  , updateStatusVoiceOptsIsDeaf    :: !Bool
  } deriving (Show, Eq)

data UpdateStatusOpts
  = UpdateStatusOpts
  { updateStatusOptsSince     :: !(Maybe UTCTime)
  , updateStatusOptsGame      :: !(Maybe Activity)
  , updateStatusOptsNewStatus :: !UpdateStatusType
  , updateStatusOptsAFK       :: !Bool
  } deriving (Show, Eq, Ord)

data Activity
  = Activity
  { activityName :: !T.Text
  , activityType :: !ActivityType
  , activityUrl  :: !(Maybe T.Text)
  } deriving (Show, Eq, Ord)

data ActivityType
  = ActivityTypeGame
  | ActivityTypeStreaming
  | ActivityTypeListening
  | ActivityTypeWatching
  deriving (Show, Eq, Ord, Enum)

data UpdateStatusType
  = UpdateStatusOnline
  | UpdateStatusDoNotDisturb
  | UpdateStatusAwayFromKeyboard
  | UpdateStatusInvisibleOffline
  | UpdateStatusOffline
  deriving (Show, Eq, Ord, Enum)

statusString :: UpdateStatusType -> T.Text
statusString s = case s of
  UpdateStatusOnline           -> "online"
  UpdateStatusDoNotDisturb     -> "dnd"
  UpdateStatusAwayFromKeyboard -> "idle"
  UpdateStatusInvisibleOffline -> "invisible"
  UpdateStatusOffline          -> "offline"

instance FromJSON GatewayReceivable where
  parseJSON = withObject "GatewayReceivable" $ \o -> do
    op <- o .: "op"
    case op :: Int of
      0  -> do etype <- o .: "t"
               ejson <- o .: "d"
               case ejson of
                 Object hm -> Dispatch <$> eventParse etype hm <*> o .: "s"
                 _other -> Dispatch (EventUnknown ("Dispatch payload wasn't an object") o)
                           <$> o .: "s"
      1  -> HeartbeatRequest . fromMaybe 0 . readMaybe <$> o .: "d"
      7  -> pure Reconnect
      9  -> InvalidSession <$> o .: "d"
      10 -> do od <- o .: "d"
               int <- od .: "heartbeat_interval"
               pure (Hello int)
      11 -> pure HeartbeatAck
      _  -> fail ("Unknown Receivable payload ID:" <> show op)

instance ToJSON GatewayReceivable where
  toJSON = \case
    Dispatch _event _int  -> undefined
    HeartbeatRequest _int -> object [ "op" .= Number 1, "d" .= Number 0]
    Reconnect             -> object ["op" .= Number 7]
    InvalidSession _bool  -> undefined
    Hello interval        -> object [ "op" .= Number 10, "d" .= object [ "heartbeat_interval" .= interval ]]
    HeartbeatAck          -> object ["op" .= Number 11]
    ParseError _err       -> undefined

-- instance FromJSON GatewaySendable where
--   parseJSON = withObject "payload" $ \o -> do
--     op <- o .: "op" :: Parser Int
--     case op of
--       1  -> Heartbeat . fromMaybe 0 . readMaybe <$> o .: "d"
--       2  -> do od <- o .: "d"
--                tok <- od .: "token"
--                compress <- od .:? "compress" .!= False
--
--       _  -> fail ("Unknown Sendable payload ID:" <> show op)

instance ToJSON GatewaySendable where
  toJSON (Heartbeat i) = object [ "op" .= (1 :: Int), "d" .= if i <= 0 then "null" else show i ]
  toJSON (Identify token compress large shard) = object [
      "op" .= (2 :: Int)
    , "d"  .= object [
        "token" .= unAuthToken token
      , "properties" .= object [
          "$os"                .= os
        , "$browser"           .= ("pipit" :: T.Text)
        , "$device"            .= ("pipit" :: T.Text)
        , "$referrer"          .= ("" :: T.Text)
        , "$referring_domain"  .= ("" :: T.Text)
        ]
      , "compress" .= compress
      , "large_threshold" .= large
      , "shard" .= shard
      ]
    ]
  toJSON (UpdateStatus (UpdateStatusOpts since game status afk)) = object [
      "op" .= (3 :: Int)
    , "d"  .= object [
        "since" .= case since of
            Nothing -> Nothing
            Just s  -> Just (10^6 * (utcTimeToPOSIXSeconds s))
      , "afk" .= afk
      , "status" .= statusString status
      , "game" .= case game of
          Nothing -> Nothing
          Just a -> Just $ object ["name" .= activityName a
                                  , "type" .= (fromEnum $ activityType a :: Int)
                                  , "url" .= activityUrl a
                                  ]
      ]
    ]
  toJSON (UpdateStatusVoice (UpdateStatusVoiceOpts guild channel mute deaf)) =
    object [
      "op" .= (4 :: Int)
    , "d"  .= object [
        "guild_id"   .= guild
      , "channel_id" .= channel
      , "self_mute"  .= mute
      , "self_deaf"  .= deaf
      ]
    ]
  toJSON (Resume token session seqId) = object [
      "op" .= (6 :: Int)
    , "d"  .= object [
        "token"      .= token
      , "session_id" .= session
      , "seq"        .= seqId
      ]
    ]
  toJSON (RequestGuildMembers (RequestGuildMembersOpts guild query limit)) =
    object [
      "op" .= (8 :: Int)
    , "d"  .= object [
        "guild_id" .= guild
      , "query"    .= query
      , "limit"    .= limit
      ]
    ]
