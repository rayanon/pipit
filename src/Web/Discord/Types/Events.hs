{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}

-- | Data structures pertaining to gateway dispatch 'Event's
module Web.Discord.Types.Events where

import           Data.Aeson
import           Data.Aeson.Casing
import           Data.Aeson.TH
import           Data.Aeson.Types
import           Data.Time                 (UTCTime)
import           Data.Time.Clock.POSIX     (posixSecondsToUTCTime)
import           Data.Time.ISO8601         (parseISO8601)

import qualified Data.Text                 as T

import           Web.Discord.Types.Channel
import           Web.Discord.Types.Common
import           Web.Discord.Types.Guild
import           Web.Discord.Types.User    (User)


data ReactionInfo
  = ReactionInfo
  { reactionUserId    :: !UserId
  , reactionGuildId   :: !(Maybe GuildId)
  , reactionChannelId :: !ChannelId
  , reactionMessageId :: !MessageId
  , reactionEmoji     :: !Emoji
  } deriving (Show, Eq)

$(deriveFromJSON (aesonDrop (length ("reaction" :: String)) snakeCase) ''ReactionInfo)
-- instance FromJSON ReactionInfo where
--   parseJSON = withObject "ReactionInfo" $ \o ->
--     ReactionInfo <$> o .:  "user_id"
--                  <*> o .:? "guild_id"
--                  <*> o .:  "channel_id"
--                  <*> o .:  "message_id"
--                  <*> o .:  "emoji"

data ReactionRemoveInfo
  = ReactionRemoveInfo
  { reactionRemoveChannelId :: !ChannelId
  , reactionRemoveGuildId   :: !GuildId
  , reactionRemoveMessageId :: !MessageId
  , reactionRemoveEmoji     :: !Emoji
  } deriving (Show, Eq)

$(deriveFromJSON (aesonDrop (length ("reactionRemove" :: String)) snakeCase) ''ReactionRemoveInfo)

-- instance FromJSON ReactionRemoveInfo where
--   parseJSON = withObject "ReactionRemoveInfo" $ \o ->
--     ReactionRemoveInfo <$> o .:  "guild_id"
--                        <*> o .:  "channel_id"
--                        <*> o .:  "message_id"
--                        <*> o .:  "emoji"

data PresenceInfo
  = PresenceInfo
  { presenceUserId  :: !UserId
  , presenceRoles   :: ![RoleId]
  -- , presenceGame :: !Maybe Activity
  , presenceGuildId :: !GuildId
  , presenceStatus  :: !T.Text
  } deriving (Show, Eq)

instance FromJSON PresenceInfo where
  parseJSON = withObject "PresenceInfo" $ \o ->
    PresenceInfo <$> (o .: "user" >>= (.: "id"))
                 <*> o .: "roles"
              -- <*> o .: "game"
                 <*> o .: "guild_id"
                 <*> o .: "status"

data TypingInfo
  = TypingInfo
  { typingUserId    :: !UserId
  , typingChannelId :: !ChannelId
  , typingTimestamp :: !UTCTime
  } deriving (Show, Eq)

instance FromJSON TypingInfo where
  parseJSON = withObject "TypingInfo" $ \o ->
    do cid <- o .: "channel_id"
       uid <- o .: "user_id"
       posix <- o .: "timestamp"
       let utc = posixSecondsToUTCTime posix
       pure (TypingInfo uid cid utc)


-- | Represents possible events sent by discord. Detailed information can be found at
-- https://discordapp.com/developers/docs/topics/gateway.
data Event
  = EventReady                      !Int !User ![Channel] ![GuildUnavailable] !T.Text
  | EventResumed                    ![T.Text]
  | EventChannelCreate              !Channel
  | EventChannelUpdate              !Channel
  | EventChannelDelete              !Channel
  | EventChannelPinsUpdate          !ChannelId !(Maybe UTCTime)
  | EventGuildCreate                !Guild !GuildInfo
  | EventGuildUpdate                !Guild
  | EventGuildDelete                !GuildUnavailable
  | EventGuildBanAdd                !GuildId !User
  | EventGuildBanRemove             !GuildId !User
  | EventGuildEmojiUpdate           !GuildId ![Emoji]
  | EventGuildIntegrationsUpdate    !GuildId
  | EventGuildMemberAdd             !GuildId !GuildMember
  | EventGuildMemberRemove          !GuildId !User
  | EventGuildMemberUpdate          !GuildId ![RoleId] User (Maybe T.Text)
  | EventGuildMemberChunk           !GuildId ![GuildMember]
  | EventGuildRoleCreate            !GuildId !Role
  | EventGuildRoleUpdate            !GuildId !Role
  | EventGuildRoleDelete            !GuildId !RoleId
  | EventMessageCreate              !Message
  | EventMessageUpdate              !ChannelId !MessageId
  | EventMessageDelete              !ChannelId !MessageId
  | EventMessageDeleteBulk          !ChannelId ![MessageId]
  | EventMessageReactionAdd         !ReactionInfo
  | EventMessageReactionRemove      !ReactionInfo
  | EventMessageReactionRemoveAll   !ChannelId !MessageId
  | EventMessageReactionRemoveEmoji !ReactionRemoveInfo
  | EventPresenceUpdate             !PresenceInfo
  | EventTypingStart                !TypingInfo
  | EventUserUpdate                 !User
  | EventUnknown                    !T.Text !Object
  -- | VoiceStateUpdate
  -- | VoiceServerUpdate
  deriving (Show, Eq)


eventString :: Event -> String
eventString = \case
  EventReady _ _ _ _ _              -> "EventReady"
  EventResumed _                    -> "EventResumed"
  EventChannelCreate _              -> "EventChannelCreate"
  EventChannelUpdate _              -> "EventChannelUpdate"
  EventChannelDelete _              -> "EventChannelDelete"
  EventChannelPinsUpdate _ _        -> "EventChannelPinsUpdate"
  EventGuildCreate _ _              -> "EventGuildCreate"
  EventGuildUpdate _                -> "EventGuildUpdate"
  EventGuildDelete _                -> "EventGuildDelete"
  EventGuildBanAdd _ _              -> "EventGuildBanAdd"
  EventGuildBanRemove _ _           -> "EventGuildBanRemove"
  EventGuildEmojiUpdate _ _         -> "EventGuildEmojiUpdate"
  EventGuildIntegrationsUpdate _    -> "EventGuildIntegrationsUpdate"
  EventGuildMemberAdd _ _           -> "EventGuildMemberAdd"
  EventGuildMemberRemove _ _        -> "EventGuildMemberRemove"
  EventGuildMemberUpdate _ _ _ _    -> "EventGuildMemberUpdate"
  EventGuildMemberChunk _ _         -> "EventGuildMemberChunk"
  EventGuildRoleCreate _ _          -> "EventGuildRoleCreate"
  EventGuildRoleUpdate _ _          -> "EventGuildRoleUpdate"
  EventGuildRoleDelete _ _          -> "EventGuildRoleDelete"
  EventMessageCreate _              -> "EventMessageCreate"
  EventMessageUpdate _ _            -> "EventMessageUpdate"
  EventMessageDelete _ _            -> "EventMessageDelete"
  EventMessageDeleteBulk _ _        -> "EventMessageDeleteBulk"
  EventMessageReactionAdd _         -> "EventMessageReactionAdd"
  EventMessageReactionRemove _      -> "EventMessageReactionRemove"
  EventMessageReactionRemoveAll _ _ -> "EventMessageReactionRemoveAll"
  EventMessageReactionRemoveEmoji _ -> "EventMessageReactionRemoveEmoji"
  EventPresenceUpdate _             -> "EventPresenceUpdate"
  EventTypingStart _                -> "EventTypingStart"
  EventUserUpdate _                 -> "EventUserUpdate"
  EventUnknown _  _                 -> "EventUnknown"


-- | Convert ToJSON value to FromJSON value
reparse :: (ToJSON a, FromJSON b) => a -> Parser b
reparse val = case parseEither parseJSON $ toJSON val of
                Left r  -> fail r
                Right b -> pure b

eventParse :: T.Text -> Object -> Parser Event
eventParse t o = case t of
    "READY"                     -> EventReady
                                   <$> o .: "v"
                                   <*> o .: "user"
                                   <*> o .: "private_channels"
                                   <*> o .: "guilds"
                                   <*> o .: "session_id"
    "RESUMED"                   -> EventResumed <$> o .: "_trace"
    "CHANNEL_CREATE"            -> EventChannelCreate <$> reparse o
    "CHANNEL_UPDATE"            -> EventChannelUpdate <$> reparse o
    "CHANNEL_DELETE"            -> EventChannelDelete <$> reparse o
    "CHANNEL_PINS_UPDATE"       -> EventChannelPinsUpdate
                                   <$> o .: "channel_id"
                                   <*> o .: "last_pin_timestamp" --parseISO8601
    "GUILD_CREATE"              -> EventGuildCreate <$> reparse o <*> reparse o
    "GUILD_UPDATE"              -> EventGuildUpdate <$> reparse o
    "GUILD_DELETE"              -> EventGuildDelete <$> reparse o
    "GUILD_BAN_ADD"             -> EventGuildBanAdd <$> o .: "guild_id" <*> o .: "user"
    "GUILD_BAN_REMOVE"          -> EventGuildBanRemove <$> o .: "guild_id" <*> o .: "user"
    "GUILD_EMOJI_UPDATE"        -> EventGuildEmojiUpdate <$> o .: "guild_id" <*> o .: "emojis"
    "GUILD_INTEGRATIONS_UPDATE" -> EventGuildIntegrationsUpdate <$> o .: "guild_id"
    "GUILD_MEMBER_ADD"          -> EventGuildMemberAdd <$> o .: "guild_id" <*> reparse o
    "GUILD_MEMBER_REMOVE"       -> EventGuildMemberRemove <$> o .: "guild_id" <*> o .: "user"
    "GUILD_MEMBER_UPDATE"       -> EventGuildMemberUpdate
                                   <$> o .: "guild_id"
                                   <*> o .: "roles"
                                   <*> o .: "user"
                                   <*> o .:? "nick"
    "GUILD_MEMBERS_CHUNK"       -> EventGuildMemberChunk <$> o .: "guild_id" <*> o .: "members"
    "GUILD_ROLE_CREATE"         -> EventGuildRoleCreate  <$> o .: "guild_id" <*> o .: "role"
    "GUILD_ROLE_UPDATE"         -> EventGuildRoleUpdate  <$> o .: "guild_id" <*> o .: "role"
    "GUILD_ROLE_DELETE"         -> EventGuildRoleDelete  <$> o .: "guild_id" <*> o .: "role_id"
    "MESSAGE_CREATE"            -> EventMessageCreate <$> reparse o
    "MESSAGE_UPDATE"            -> EventMessageUpdate     <$> o .: "channel_id" <*> o .: "id"
    "MESSAGE_DELETE"            -> EventMessageDelete     <$> o .: "channel_id" <*> o .: "id"
    "MESSAGE_DELETE_BULK"       -> EventMessageDeleteBulk <$> o .: "channel_id" <*> o .: "ids"
    "MESSAGE_REACTION_ADD"      -> EventMessageReactionAdd <$> reparse o
    "MESSAGE_REACTION_REMOVE"   -> EventMessageReactionRemove <$> reparse o
    "MESSAGE_REACTION_REMOVE_ALL" -> EventMessageReactionRemoveAll
                                     <$> o .: "channel_id"
                                     <*> o .: "message_id"
    "MESSAGE_REACTION_REMOVE_EMOJI" -> EventMessageReactionRemoveEmoji <$> reparse o
    "PRESENCE_UPDATE"           -> EventPresenceUpdate <$> reparse o
    "TYPING_START"              -> EventTypingStart <$> reparse o
    "USER_UPDATE"               -> EventUserUpdate <$> reparse o
 -- "VOICE_STATE_UPDATE"        -> EventVoiceStateUpdate          <$> reparse o
 -- "VOICE_SERVER_UPDATE"       -> EventVoiceServerUpdate         <$> reparse o
    _other_event                -> EventUnknown t <$> reparse o
