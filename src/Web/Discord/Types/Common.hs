{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}

-- | Provides base types and utility functions needed for modules in Discord.Internal.Types
module Web.Discord.Types.Common where

-- import           Control.Monad         (mzero)
import           Data.Aeson
-- import           Data.Bifunctor        (first)
import           Data.Bits
-- import           Data.Functor.Compose  (Compose (Compose, getCompose))
-- import           Data.Monoid           ((<>))
import           Data.String           (IsString)
import           Data.Time.Clock
import           Data.Time.Clock.POSIX
import           Data.Word
import           Text.Read             (readMaybe)

import qualified Data.Text             as T

-- | Authorization token for the Discord API
newtype AuthToken
  = AuthToken { unAuthToken :: T.Text }
  deriving (Show, Eq, Ord, IsString)

-- | Get the raw token formatted for use with the websocket gateway
mkAuthToken :: AuthToken -> T.Text
mkAuthToken (AuthToken tok) =
  let token = T.strip tok
      prefix = if "Bot " `T.isPrefixOf` token then "" else "Bot "
  in prefix <> token

-- | A unique integer identifier. Can be used to calculate the creation date of an entity.
newtype Snowflake = Snowflake { unSnowflake ::  Word64 }
  deriving (Ord, Eq, Num, Integral, Enum, Real, Bits, Show)

snowflakeToString :: Snowflake -> String
snowflakeToString = show . unSnowflake

instance FromJSON Snowflake where
  parseJSON = withText "stringified Snowflake" $ \v ->
    case readMaybe (T.unpack v) of
      Just val -> pure $ Snowflake val
      Nothing  -> fail "Cannot parse Snowflake. Not a stringified Word64."

instance ToJSON Snowflake where
  toJSON = String . T.pack . show . unSnowflake

newtype ChannelId = ChannelId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

newtype GuildId = GuildId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON, Num)

newtype MessageId = MessageId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

newtype EmojiId = EmojiId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

newtype UserId = UserId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

newtype OverwriteId = OverwriteId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

newtype RoleId = RoleId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

newtype IntegrationId = IntegrationId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

newtype WebhookId = WebhookId Snowflake
  deriving (Show, Eq, FromJSON, ToJSON)

-- | Gets a creation date from a snowflake.
snowflakeCreationDate :: Snowflake -> UTCTime
snowflakeCreationDate x = posixSecondsToUTCTime . realToFrac
  $ 1420070400 + quot (shiftR x 22) 1000

-- | Default timestamp
epochTime :: UTCTime
epochTime = posixSecondsToUTCTime 0
