module Web.Discord.Logging where

import           Control.Monad.IO.Class
import           Data.ByteString        (ByteString)
import qualified Data.ByteString.Char8  as BC
import           Data.Time.Clock
import           Prelude                hiding (log)

logDebug :: MonadIO m => ByteString -> m ()
logDebug = logWithLevel "DEBUG"

logInfo :: MonadIO m => ByteString -> m ()
logInfo = logWithLevel "INFO"

logError :: MonadIO m => ByteString -> m ()
logError = logWithLevel "ERROR"

logWithLevel :: MonadIO m => ByteString -> ByteString -> m ()
logWithLevel level m = log $ "[" <> level <> "] " <> m

log :: MonadIO m => ByteString -> m ()
log m = do
  currTime <- liftIO getCurrentTime
  liftIO $ BC.putStrLn $ "[" <> BC.pack (show currTime) <> "] " <> m
