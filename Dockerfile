FROM debian:bullseye-slim
WORKDIR /pipit
RUN apt-get -y update \
  && apt-get install -y ca-certificates \
  && apt-get clean \
  && apt-get autoremove \
  && rm -rf /var/lib/apt/lists/*
ARG project_bin
COPY ${project_bin} /pipit
CMD ["/pipit/pipit"]
