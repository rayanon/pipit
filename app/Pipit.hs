{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Pipit where

import           Data.Text         (Text)
import           Web.Discord.Types

import qualified Data.Text         as T
import qualified System.Random     as Random

data BotCommand
  = BCHelp
  | BCPing
  | BCBeer
  | BCPoll !Text ![Text]
  | BCHoogle !Text
  | BCWiki !Text
  | BCMarkov
  | BCLiveScores
  | BCIgnore
  deriving (Show)

prefix :: Text
prefix = "!"

-- runParser :: P.ReadP a -> String -> Maybe a
-- runParser parser str = case reverse $ P.readP_to_S parser str of
--   []          -> Nothing
--   ((res,_):_) -> Just res

-- parsePollDetails = P.many parsePollDetail
-- parsePollDetail = P.between (P.char '"') (P.char '"') (P.many1 $ P.satisfy (\c -> c `elem` alnum))

-- alnum = ['A'..'Z'] <> ['a'..'z'] <> (map intToDigit [1..9]) <> ['?', '\'', ';', '!','@','#','$','%','^','&','*','(',')',':']


hoogleUsage :: Text
hoogleUsage = "**__Hoogle Usage__**\n`!hoogle <search-term>`\nExamples:  `!hoogle map` ,  `!hoogle a -> b -> a` etc."

wikiUsage :: Text
wikiUsage = "**__Wikipedia Search Usage__**\n`!wiki <search-term>`\nExamples:  `!wiki history of football` etc."

pollUsage :: Text
pollUsage = "**__Poll Usage__**\n`!poll <question>`\nExamples:  `!poll Is Pipit bot good?` etc."

parseCommand :: (Text, Message) -> Either Text BotCommand
parseCommand (t, _message)
  | t == prefix <> "help"   = pure BCHelp
  | t == prefix <> "ping"   = pure BCPing
  | t == prefix <> "pipit"  = pure BCMarkov
  | t == prefix <> "scores" = pure BCLiveScores

  | T.isPrefixOf (prefix <> "beer") t   = pure BCBeer
  | T.isPrefixOf (prefix <> "hoogle") t = case T.stripPrefix (prefix <> "hoogle ") t of
      Just query -> if T.null query then (Left hoogleUsage) else pure $ BCHoogle query
      Nothing    -> Left hoogleUsage
  | T.isPrefixOf (prefix <> "poll") t   = case T.stripPrefix (prefix <> "poll ") t of
      Just pollDetails -> if T.null pollDetails then (Left pollUsage) else pure $ BCPoll pollDetails []
      Nothing          -> Left pollUsage
  | T.isPrefixOf (prefix <> "wiki") t   = case T.stripPrefix (prefix <> "wiki ") t of
      Just searchTerm -> if T.null searchTerm then (Left wikiUsage) else pure $ BCWiki searchTerm
      Nothing         -> Left wikiUsage
  | otherwise = pure BCIgnore

thumbsUpEmoji, thumbsDownEmoji, eyesEmoji :: Text
thumbsUpEmoji = "👍"
thumbsDownEmoji = "👎"
eyesEmoji = "👀"

pongMessages :: [Text]
pongMessages =
  [ "I am tired of being online all day :unamused:"
  , "Pong!"
  , "What do you want from me?"
  , "I am always online, always reachable. Unlike you humans."
  ]

beerQuotes :: [Text]
beerQuotes =
  [ "Work is the curse of the drinking classes. ― Oscar Wilde"
  , "I don't think I've drunk enough beer to understand that. ― Terry Pratchett"
  , "A man who lies about beer makes enemies ― Stephen King"
  , "Here here, have some chilled beer, my good fellow! :beers:"
  ]

getRandomMessage :: [Text] -> IO Text
getRandomMessage xs = do
  idx <- Random.randomRIO (0, length xs - 1)
  return $ xs !! idx
