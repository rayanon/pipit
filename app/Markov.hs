{-# LANGUAGE OverloadedStrings #-}
module Markov
  ( runMarkov
  ) where

import qualified Data.HashMap.Strict as Map
import           Data.Text           (Text, split)
import qualified System.Random       as Random

randomItem :: [a] -> IO a
randomItem [] = error "randomItem called on empty list"
randomItem xs = do
  idx <- Random.randomRIO (0, length xs - 1)
  return $ xs !! idx

tokenizeCorpus :: Text -> [(Int, Text)]
tokenizeCorpus = zip [0..] . split (\c -> c == ' ' || c == '\n')

generateTable :: [(Int, Text)] -> Map.HashMap Text [Text]
generateTable corpus =
  foldr (\(idx, word) table -> updateTable table word (getNextWord idx)) Map.empty corpus
  where
    getNextWord curIdx =
      if curIdx + 1 < length corpus
      then Just (snd $ corpus !! (curIdx + 1))
      else Nothing

    updateTable tbl word nextWordIf = case Map.lookup word tbl of
      Nothing       -> maybe tbl (\nw -> Map.insert word [nw] tbl) nextWordIf
      Just existing -> maybe tbl (\nw -> Map.insert word (nw:existing) tbl) nextWordIf

generateMarkov :: Int -> Text -> Map.HashMap Text [Text] -> IO Text
generateMarkov size startWord table = do
  generateChain (startWord, startWord) size
  where
    generateChain (result, currentWord) currCount
      | currCount == 0 = return result
      | otherwise = do
          nextWord <- chooseNextWord currentWord
          case nextWord of
            Nothing    -> return result
            Just word' -> generateChain (result <> " " <> word', word') (currCount - 1)

    chooseNextWord currentWord = do
      case Map.lookup currentWord table of
        Nothing -> pure Nothing
        Just ps -> if null ps then pure Nothing else Just <$> randomItem ps

-- | Run a markov chain to generate random text of given size, from a given corpus of text
-- Pass number of words and the corpus text to generate a piece of random text
-- >>> runMarkov 20 "roses are red violets are blue tulips are red"
runMarkov :: Int -> Text -> IO Text
runMarkov size rawCorpus = do
  let corpus = tokenizeCorpus rawCorpus
      table = generateTable corpus
  startWord <- snd <$> randomItem corpus
  generateMarkov size startWord table
