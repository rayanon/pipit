{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import           Control.Concurrent   (threadDelay)
import           Control.Monad.Reader
import           Data.Maybe           (catMaybes, fromMaybe, listToMaybe)
import           Data.Text            (Text)

import qualified Data.Text            as T
import qualified System.Environment   as Sys
import qualified System.Random        as Random

import           Markov
import           Pipit
import           Pipit.Hoogle
import           Pipit.Wiki
import           Web.Discord
import           Web.Discord.Gateway
import           Web.Discord.Types

botTokenEnv :: String
botTokenEnv = "PIPIT_BOT_TOKEN"

-- the #reddit-posts channel in my server
redditPostsChannel :: ChannelId
redditPostsChannel = ChannelId (Snowflake 709898546014322749)

-- the #reddit-whatsthisbird-india channel in my server
redditWTBIndiaChannel :: ChannelId
redditWTBIndiaChannel = ChannelId (Snowflake 742324100705550366)

main :: IO ()
main = do
  env <- Sys.lookupEnv botTokenEnv
  let token = fromMaybe (error $ "cannot find required env: " <> botTokenEnv) env
  runDiscordWSClient token $ DiscordHandlers onMessage onReaction

onReaction :: (MonadReader DiscordContext m) => Message -> m ()
onReaction = const $ return ()

onMessage :: (MonadReader DiscordContext m, MonadIO m) => Message -> m ()
onMessage message
  | channel == redditPostsChannel = handleRedditMessages message
  | otherwise =
  case parseCommand (messageText message, message) of
    Left err  -> sendMessage_ channel err [] Nothing
    Right cmd -> case cmd of
      BCHelp                  -> sendHelp channel
      BCPing                  -> sendPong channel message authorId
      BCBeer                  -> passBeer channel message authorId
      BCPoll question options -> conductPoll channel question options
      BCHoogle query          -> handleHoogle channel query
      BCWiki query            -> handleWikiSearch channel query
      BCMarkov                -> handleMarkov channel message
      BCLiveScores            -> handleLiveScores channel
      BCIgnore                -> return ()
  where
    channel = messageChannel message
    -- The author (Discord user) of the current message
    authorId = userId $ messageAuthor message

sendPong :: (MonadIO m, MonadReader DiscordContext m) => ChannelId -> Message -> UserId -> m ()
sendPong channel message authorId = do
  msg <- liftIO $ getRandomMessage pongMessages
  addReaction channel (messageId message) eyesEmoji
    >> sendMessage_ channel msg [authorId] Nothing

-- pass a beer to the mentioned people; otherwise to the author of the
-- message
passBeer :: (MonadIO m, MonadReader DiscordContext m) => ChannelId -> Message -> UserId -> m ()
passBeer channel message authorId = do
  msg <- liftIO $ getRandomMessage beerQuotes
  let mentions =
        if null (messageMentions message)
        then [authorId]
        else map userId $ messageMentions message
  sendMessage_ channel msg mentions Nothing
    >> sendMessage_ channel ":beer:" [] Nothing

handleMarkov :: (MonadReader DiscordContext m, MonadIO m) => ChannelId -> Message -> m ()
handleMarkov channel message = do
  triggerTypingIndicator channel
  -- get the last 100 messages from the `!markov` command, in the same channel
  msgs <- getMessages channel (messageId message) MCDirectionBefore 100
  -- filter the messages: a) ignore bot commands b) ignore links
  let notCommand t = not $ "!" `T.isPrefixOf` t
      notLink t    = not $ "http" `T.isPrefixOf` t
      corpus       = T.unlines
                   $ take 30
                   $ filter (\t -> notCommand t && notLink t)
                   $ map messageText
                   $ filter (\m -> userUsername (messageAuthor m) /= "pipit")
                   $ message:msgs
  -- pick a random num of words to generate the text
  randomSize <- liftIO $ Random.randomRIO (5, 30)
  -- generate the text
  result <- liftIO $ runMarkov randomSize corpus
  sendMessage_ channel result [] Nothing

handleLiveScores :: (MonadReader DiscordContext m, MonadIO m) => ChannelId -> m ()
handleLiveScores channel = do
  -- TODO: fetch live cricket and football live scores from external APIs
  -- and return results
  sendMessage_ channel "Live sports scores coming soon! Stay tuned! :smiley:" [] Nothing

conductPoll :: (MonadReader DiscordContext m, MonadIO m) => ChannelId -> Text -> [Text] -> m ()
conductPoll channel question options
  | T.null question = return ()
  | null options = do
      msg <- sendMessage channel (":bar_chart: **" <> question <> "**") [] Nothing
      let msgId= messageId msg
      addReaction channel msgId thumbsUpEmoji
      liftIO $ threadDelay 500000 -- half-second = 0.5 * 10 ^ 6
      addReaction channel msgId thumbsDownEmoji
  | otherwise = sendMessage_ channel "I am not ready for option-based poll yet. Sorry! :frowning:" [] Nothing

handleRedditMessages :: (MonadReader DiscordContext m, MonadIO m) => Message -> m ()
handleRedditMessages message = do
  let keywords = [ "india"
                 , "bangladesh"
                 , "nepal"
                 , "bhutan"
                 , "sri lanka"
                 , "pakistan"
                 , "afghanistan"
                 , "iran"
                 , "iraq"
                 , "turkey"
                 , "poland"
                 , "italy"
                 , "france"
                 , "germany"
                 , "myanmar"
                 , "thailand"
                 , "vietnam"
                 , "malaysia"
                 , "indonesia"
                 , "philippines"
                 , "brunei"
                 , "china"
                 , "australia"
                 ]
  let corpus = T.toLower $ T.unlines
             $ catMaybes ([embedTitle, embedDescription, embedUrl] <*> messageEmbeds message)
  -- logDebug $ txtToBs $ "message in #reddit-posts. This is our corpus: " <> corpus
  when (any (`T.isInfixOf` corpus) keywords) $
    sendMessage_ redditWTBIndiaChannel
      (messageText message)
      (map userId $ messageMentions message)
      (listToMaybe $ messageEmbeds message)

sendHelp :: (MonadReader DiscordContext m, MonadIO m) => ChannelId -> m ()
sendHelp channel = sendMessage_ channel "" [] (Just helpEmbed)
  where
    helpEmbed = makeEmbed "Pipit Help" "http://github.com/ecthiender/pipit" helpFields color
    color = 15105570 -- orange https://gist.github.com/thomasbnt/b6f455e2c7d743b796917fa3c205f812
    helpFields =
      [ EmbedField ":small_orange_diamond: !help" "Print this help" Nothing
      , EmbedField ":small_orange_diamond: !ping" "Ping pong! Whatever!" Nothing
      , EmbedField ":small_orange_diamond: !beer" "Passes a beer.\n`!beer` to get a beer for yourself. `!beer @mention` to pass a beer to someone." Nothing
      , EmbedField ":small_orange_diamond: !poll" "Conducts a poll. Currently only supports questions with Yes/No poll.\n `!poll Is pipit a good bot?`. " Nothing
      , EmbedField ":small_orange_diamond: !hoogle" "Run a Hoogle search.\n `!hoogle foldr` or `!hoogle a -> b -> a`" Nothing
      , EmbedField ":small_orange_diamond: !wiki" "Run a Wikipedia search.\n `!wiki <search term>`" Nothing
      , EmbedField ":small_orange_diamond: !pipit" "I will try to speak like you.\n Usage: `!pipit`" Nothing
      ]


data DiscordHandle

data DiscordOptions
  = DiscordOptions
  { discordToken               :: Text
  , discordOnStart             :: DiscordHandle -> IO ()
  , discordOnEnd               :: IO ()
  , discordOnEvent             :: DiscordHandle -> Event -> IO ()
  , discordOnLog               :: Text -> IO ()
  , discordForkThreadForEvents :: Bool
  }

-- | EXAMPLE: we want to have
-- | Replies "pong" to every message that starts with "ping"
-- pingpongExample :: IO ()
-- pingpongExample = do
--   userFacingError <- runDiscord $ def
--                      { discordToken = "Bot xxxx"
--                      , discordOnEvent = eventHandler
--                      }
--   TIO.putStrLn userFacingError

-- eventHandler :: DiscordHandle -> Event -> IO ()
-- eventHandler dis event = case event of
--   EventMessageCreate m ->
--     when (not (fromBot m) && isPing (messageText m)) $ do
--       undefined
--       -- void $ restCall dis (R.CreateReaction (messageChannel m, messageId m) "eyes")
--       threadDelay (4 * 10^6)
--       -- _ <- restCall dis (R.CreateMessage (messageChannel m) "Pong!")
--       undefined
--       pure ()
--   _ -> pure ()

-- fromBot :: Message -> Bool
-- fromBot m = userIsBot (messageAuthor m)

-- isPing :: Text -> Bool
-- isPing = ("ping" `isPrefixOf`) . toLower
