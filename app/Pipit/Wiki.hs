{-# LANGUAGE OverloadedStrings #-}
module Pipit.Wiki where

import           Control.Monad.IO.Class
import           Control.Monad.Reader
import           Data.Text              (Text)
import           Network.HTTP.Req       ((/:), (=:))

import qualified Data.Aeson             as A
import qualified Data.Aeson.Types       as A
import qualified Data.Text              as T
import qualified Data.Vector            as V
import qualified Network.HTTP.Req       as Req

import           Web.Discord
import           Web.Discord.Gateway
import           Web.Discord.Types

type WikiApiSearchResult = [WikiApiSearchResultItem]

data WikiApiSearchResultItem
  = WSRIString !Text
  | WSRIList ![Text]
  deriving (Show)

instance A.FromJSON WikiApiSearchResultItem where
  parseJSON j = case j of
    A.String t -> pure $ WSRIString t
    A.Array xs -> (pure . WSRIList . V.toList) =<< mapM parseText xs
    _          -> fail $ "unexpected Wiki response: " <> show j
    where
      parseText :: A.Value -> A.Parser Text
      parseText (A.String v) = pure v
      parseText _            = fail "expecting String value"

type WikiSearchResult = [(Text, Text)]

runWikiSearch :: Text -> IO WikiSearchResult
runWikiSearch q = do
  -- https://en.wikipedia.org/w/api.php?action=opensearch&search=<search>
  let url = Req.https "en.wikipedia.org" /: "w" /: "api.php"
      options = ("search" :: Text) =: q
             <> ("action" :: Text) =: ("opensearch" :: Text)
  Req.runReq Req.defaultHttpConfig $ do
    resp <- Req.req Req.GET url Req.NoReqBody Req.jsonResponse options
    let resBody = Req.responseBody resp
    -- liftIO $ putStrLn $ "[DEBUG] Wiki search response body ====>>>> " <> show resBody
    let val = map isList $ resBody
    return $ zip (val !! 1) (val !! 3)
  where
    isList x = case x of
      WSRIString _ -> []
      WSRIList xs  -> xs

handleWikiSearch :: (MonadReader DiscordContext m, MonadIO m) => ChannelId -> Text -> m ()
handleWikiSearch cid q = do
  res <- take 3 <$> liftIO (runWikiSearch q)
  let msg = T.unlines $ flip map res $ \(name, link) ->
        ":small_orange_diamond: " <> name <> " : " <> link
  sendMessage_ cid msg [] Nothing
