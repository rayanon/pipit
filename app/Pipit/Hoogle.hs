{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Pipit.Hoogle where

import           Control.Monad.Reader
-- import           Control.Monad.IO.Class
import           Data.Text            (Text)

import qualified Data.Aeson           as A
import qualified Data.Aeson.Casing    as A
import qualified Data.Aeson.TH        as A
import qualified Data.Text            as T
import qualified Network.HTTP.Req     as Req
import qualified Network.HTTP.Types   as HTTP

import           Web.Discord
import           Web.Discord.Gateway
import           Web.Discord.Types

data HoogleResource
  = HoogleResource
  { hrUrl  :: !Text
  , hrName :: !Text
  } deriving (Show)

$(A.deriveJSON (A.aesonDrop 2 A.snakeCase) {A.omitNothingFields = True} ''HoogleResource)

data HoogleItem
  = HoogleItem
  { hiUrl     :: !Text
  , hiItem    :: !Text
  , hiDocs    :: !Text
  , hiModule  :: !HoogleResource
  , hiPackage :: !HoogleResource
  } deriving (Show)

$(A.deriveJSON (A.aesonDrop 2 A.snakeCase) {A.omitNothingFields = True} ''HoogleItem)

type HoogleResult = [HoogleItem]

runHoogle :: Text -> IO HoogleResult
runHoogle q = do
  -- "https://hoogle.haskell.org?mode=json&format=text&hoogle=map&start=1&count=2
  let url = Req.https "hoogle.haskell.org"
      options = ("hoogle" :: Text) Req.=: q
             <> ("mode"   :: Text) Req.=: ("json" :: Text)
             <> ("format" :: Text) Req.=: ("text" :: Text)
             <> ("start"  :: Text) Req.=: ("1" :: Text)
             <> ("count"  :: Text) Req.=: ("3" :: Text)
  Req.runReq Req.defaultHttpConfig $ do
    jsonn <- Req.req Req.GET url Req.NoReqBody Req.jsonResponse options
    let val = Req.responseBody jsonn
    return val

handleHoogle :: (MonadReader DiscordContext m, MonadIO m) => ChannelId -> Text -> m ()
handleHoogle cid q = do
  searchResult <- liftIO $ runHoogle q
  let embed = renderEmbed searchResult
  sendMessage_ cid "" [] (Just embed)
  where
    safeHead []    = Nothing
    safeHead (x:_) = Just x

    renderEmbed searchResult =
      let footer = EmbedField "More results at" ("https://hoogle.haskell.org?hoogle=" <> encodedQuery) Nothing
      in hoogleEmbed { embedFields = map renderEmbedField searchResult <> [footer] }

    renderEmbedField item =
      let packageName = hrName . hiPackage $ item
          packageUrl = hrUrl . hiPackage $ item
          moduleName = hrName . hiModule $ item
          fullUrl    = hiUrl item
          itemName = safeHead $ T.words $ hiItem item
          title = maybe q (\n -> moduleName <> "." <> n) itemName
          desc = T.unlines [ ""
                           , ":package:  **Package:** [" <> packageName <> "](" <> packageUrl <> ")"
                             <> " | :envelope:  **Module:** [" <> moduleName <> "](" <> fullUrl <> ")"
                           , "```haskell\n" <> hiItem item <> "```"
                           , hiDocs item
                           ]
      in EmbedField title desc Nothing

    hoogleEmbed = Embed
                  { embedAuthor      = Nothing
                  , embedTitle       = Just ":arrow_down:   **__Hoogle Result__**   :arrow_down:"
                  -- ^ Title of the embed
                  , embedUrl         = Just ("https://hoogle.haskell.org?hoogle=" <> encodedQuery)
                  -- ^ URL of embed
                  , embedThumbnail   = Nothing
                  -- ^ Thumbnail in top-right
                  , embedDescription = Nothing
                  -- ^ Description of embed
                  , embedFields      = []
                  -- ^ Fields of the embed
                  , embedImage       = Nothing
                  , embedFooter      = Nothing
                  , embedColor       = Just 15105570
                  -- ^ The embed color: orange
                  -- https://gist.github.com/thomasbnt/b6f455e2c7d743b796917fa3c205f812
                  , embedTimestamp   = Nothing
                  -- ^ The time of the embed content
                  , embedType        = Just "rich"
                  -- ^ Type of embed (Always "rich" for users)
                  , embedVideo       = Nothing
                  -- ^ Only present for "video" types
                  , embedProvider    = Nothing
                  -- ^ Only present for "video" types
                }
    encodedQuery = bsToTxt $ HTTP.urlEncode True (txtToBs q)
